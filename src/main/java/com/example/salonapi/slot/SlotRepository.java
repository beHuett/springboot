package com.example.salonapi.slot;

import com.example.salonapi.slot.Slot;

import org.springframework.data.repository.CrudRepository;

public interface SlotRepository extends CrudRepository<Slot, Long> {


    Slot findById(long id);
}
