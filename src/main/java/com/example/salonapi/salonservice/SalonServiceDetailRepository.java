package com.example.salonapi.salonservice;


import com.example.salonapi.salonservice.SalonServiceDetail;
import org.springframework.data.repository.CrudRepository;

public interface SalonServiceDetailRepository extends CrudRepository<SalonServiceDetail, Long> {

}
