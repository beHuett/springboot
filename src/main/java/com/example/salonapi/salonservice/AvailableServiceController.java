package com.example.salonapi.salonservice;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/services/retrieveAvailableSalonServices",
    produces = "application/json")
@CrossOrigin(origins = "*")
public class AvailableServiceController {
}
